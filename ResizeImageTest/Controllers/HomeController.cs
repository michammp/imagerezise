﻿using Microsoft.AspNetCore.Mvc;
using ResizeImageTest.Models;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;
using System.Diagnostics;

namespace ResizeImageTest.Controllers
{
	public class HomeController : Controller
	{
		private readonly ILogger<HomeController> _logger;

		public HomeController(ILogger<HomeController> logger)
		{
			_logger = logger;
		}

		public IActionResult Index()
		{
			return View();
		}

		[HttpPost]
		public IActionResult Index(IFormFile file)
		{
			string fileName = string.Empty;
			string path = string.Empty;
			if (file.Length > 0)
			{
				fileName = Guid.NewGuid() + Path.GetExtension(file.FileName);
				path = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/upload"));
				string fullPath = Path.Combine(path, fileName);
				using (var image = Image.Load(file.OpenReadStream()))
				{
					string newSize = ImageResize(image, 600, 600);
					string[] sizeArray = newSize.Split(',');
					image.Mutate(x => x.Resize(Convert.ToInt32(sizeArray[1]), Convert.ToInt32(sizeArray[0])));
					image.Save(fullPath);
					TempData["msg"] = "File uploaded successfully";
				}
			}
			return View();
		}

		public IActionResult Privacy()
		{
			return View();
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}

		public string ImageResize(Image img, int MaxWidth, int MaxHeight)
		{
			if(img.Width > MaxWidth || img.Height > MaxHeight)
			{
				double widthRatio = (double)img.Width / (double)MaxWidth;
				double heightRatio = (double)img.Height / (double)MaxHeight;
				double ratio = Math.Max(widthRatio, heightRatio);
				int newWidth = (int)(img.Width / ratio);
				int newHeight = (int)(img.Height / ratio);
				return newHeight.ToString() + "," + newWidth.ToString();
			}
			else
			{
				return img.Height.ToString() + "," + img.Width.ToString();
			}
		}
	}
}